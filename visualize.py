import cv2
import os
import pandas as pd
from collections import defaultdict
from tqdm import tqdm


def visualize_annotations(h5_file, dbname, output_dir, corner_columns=None, query=None):
    if corner_columns is None:
        raise ValueError('The columns corresponding to the four corners of a bounding box must be provided.')
    df = pd.read_hdf(h5_file, key=dbname)
    df.set_index('image', inplace=True)
    if query is not None:
        df = df.query(query)
    df = df.groupby(df.index).aggregate(lambda x: list(x))
    df_dict = df.to_dict(orient='index', into=defaultdict())
    images = list(df_dict.keys())
    xmin, ymin, xmax, ymax = corner_columns
    for image in tqdm(images):
        filename = os.path.basename(image)
        savefilename = os.path.join(output_dir, filename)
        num_annotations = len(df_dict[image][xmin])
        if not (len(df_dict[image][xmin]) == len(df_dict[image][xmax]) == len(df_dict[image][ymin]) == len(df_dict[image][ymax])):
            raise ValueError('There is some mistake with the annotations.')
        img = cv2.imread(image)
        for ann in range(num_annotations):
            bbox_xmin = int(df_dict[image][xmin][ann])
            bbox_xmax = int(df_dict[image][xmax][ann])
            bbox_ymin = int(df_dict[image][ymin][ann])
            bbox_ymax = int(df_dict[image][ymax][ann])
            img = cv2.rectangle(img, (bbox_xmin, bbox_ymin), (bbox_xmax, bbox_ymax), color=(0,0,255), thickness=2)
        cv2.imwrite(savefilename, img)
