import pandas as pd
from collections import defaultdict
import time
# This is how you can get information about what datasets are stored.
store = pd.HDFStore('/data/stars/user/uujjwal/PhD-Projects/DetectionDatasetManagement/detection-dataframes.h5')
datasets = store.keys()
print(datasets)
store.close()

# This reads the training dataset of caltech (10x means frames taken 3 frames apart. This is ususally used for training
pd = pd.read_hdf('/data/stars/user/uujjwal/PhD-Projects/DetectionDatasetManagement/detection-dataframes.h5',
                 key='caltech10xtrain')

# Now we extract caltech reasonable from it
# For caltech reasonable, height >= 50 and occlusion <=0.35
pd_reasonable = pd.query('height>=50 and occluded<=0.35')


pd_reasonable.set_index('image', inplace=True)

pd_reasonable = pd_reasonable.groupby(pd_reasonable.index).aggregate(lambda x: list(x))

# Now you can get the annotations very easily. The detailed list of the column names are as follows
# 'category' : Name of the object category
#        'occluded' : A number between 0 and 1. Denotes the amount of occlusion.
#        'height' : Height of the bounding box
#        'width' : width of the bounding box.
#        'height_vis' : height of the visible portion of the bounding box
#        'width_vis' : width of the visible portion of the bounding box
#        'aspect_ratio : Aspect ratio of the object (width/height)
#        'xmin' : Top left corner ( x coordinate) of the bounding box
#        'ymin' : Top left corner (y coordinate) of the bounding box
#        'xmax' : Bottom right corner (x coordinate of the bounding box
#        'ymax' : Bottom right corner (y coordinate of the bounding box
#        'xmin_vis', 'ymin_vis', 'xmax_vis', 'ymax_vis' : Same as xmin, ymin, xmax, ymax
#        but only for the visible part of the bounding box. If the person is not occluded, then these
#        will be the same as xmin, ymin, xamx , ymax.

xmin = pd_reasonable['xmin']
ymin = pd_reasonable['ymin']

# And so on

pdd = pd_reasonable.to_dict(orient='index', into=defaultdict())

pdd_keys = list(pdd.keys())

print(len(pdd_keys))

for key in pdd_keys:
    print(pdd[key]['xmin'])
    print('----------------')
    print(pd_reasonable.loc[pd_reasonable.index == key].xmin.tolist())
    print('xxxxxxxxxxxxxxxxx')
    time.sleep(0.3)
 
