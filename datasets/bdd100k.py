import glob
import json
import os

from natsort import natsort_keygen
from tqdm import tqdm

from datasets.detection_dataset import DetectionDataset


class BDD100K(DetectionDataset):
    def __init__(self, base_dir):
        super(BDD100K, self).__init__('BDD100K')
        self._base_dir = base_dir

    def _getfiles(self, subset):
        """
        Returns the lists of image files and corresponding label files of bdd100K dataset.
        :param base_dir: string. Base folder inside which all the BDD100K dataset is stored.
        :param subset: string. One of 'train', 'test' and 'val'.
        :return: Dict. It has two keys. The key 'images' contains a list of image filenames with full path.
                       The key 'labels' contains a list of label filenames with full path. If subset='test', the key
                       'labels' contains an empty list.
        """
        if not os.path.exists(self._base_dir):
            raise ValueError('The path {} does not exist.'.format(
                self._base_dir))

        if subset not in ['train', 'test', 'val']:
            raise ValueError('subset must be one of train, test or  val.')

        natsort_key = natsort_keygen(
            key=lambda x: os.path.splitext(os.path.basename(x))[0])

        image_path = os.path.join(self._base_dir, 'images', '100k', subset)

        images = glob.glob(os.path.join(image_path, '*.jpg'))
        images.sort(key=natsort_key)

        if subset in ['train', 'val']:
            label_path = os.path.join(self._base_dir, 'labels', '100k', subset)
            labels = glob.glob(os.path.join(label_path, '*.json'))
            labels.sort(key=natsort_key)
        else:
            labels = []

        output = {
            'images': images,
            'labels': labels
        }
        return output

    @staticmethod
    def _read_json_single(jsonfile):
        """
        Reads a JSON annotation file for BDD100K dataset and returns the following annotations --
        'object category names', 'xmin','ymin','xmax','ymax',
        'occluded bools' (True if object was occluded), 'truncated bools' (True if the object was truncated).
        :param jsonfile: Full path to the JSON file for annotation
        :return: A tuple  (dict, string, string, string). The dictionary has the following structure :
         { 'category' : List of object category names,
            'xmin' : List of top left x coordinates.
            'ymin' : List of top left y coordinates.
            'xmax' : List of bottom right x coordinates.
            'ymax' : List of bottom right y coordinates.
            'occluded' : List of bools. If True, the object was occluded.
            'truncated' : List of bools. If True, the object was truncated due to being on the boundary.
            'height' : List of floats. Height of annotation.
            'width' : List of floats. Width of annotation.
            'aspect_ratio : List of floats. Aspect ratio of annotation (
            width/height)
         }
         The remaining three strings are : scene (such as residential), timeofday (such as daytime), weather (such as
         overcast)
        """
        with open(jsonfile, 'r') as fid:
            data = json.load(fid)

        objects = data['frames'][0]['objects']

        annotations = {
            'category': [],
            'occluded': [],
            'truncated': [],
            'height': [],
            'width': [],
            'aspect_ratio': [],
            'xmin': [],
            'ymin': [],
            'xmax': [],
            'ymax': []
        }

        for obj in objects:
            if 'box2d' not in obj.keys():
                continue

            xmin = obj['box2d']['x1']
            xmax = obj['box2d']['x2']
            ymin = obj['box2d']['y1']
            ymax = obj['box2d']['y2']
            height = ymax - ymin + 1
            width = xmax - xmin + 1
            if not (width or height):
                continue
            annotations['xmin'].append(xmin)
            annotations['xmax'].append(xmax)
            annotations['ymin'].append(ymin)
            annotations['ymax'].append(ymax)
            annotations['category'].append(obj['category'])
            annotations['occluded'].append(obj['attributes']['occluded'])
            annotations['truncated'].append(obj['attributes']['truncated'])
            aspect_ratio = float(width) / float(height)
            annotations['height'].append(height)
            annotations['width'].append(width)
            annotations['aspect_ratio'].append(aspect_ratio)

        scene = data['attributes']['scene']
        timeofday = data['attributes']['timeofday']
        weather = data['attributes']['weather']
        return annotations, scene, timeofday, weather

    def assemble_annotations(self, subset):
        """
        Wrapper function assembling all the annotation information for mongodb. For the 'test' subset it will return an
        empty list.
        :param base_dir: string. Base folder inside which the BDD100K dataset is stored.
        :param subset: string. Must be one of 'train', 'test' and 'val'.
        :return: A list of dictionaries. Each dictionary has the following structure :
         { 'category' : object category names,
            'xmin' : List of top left x coordinates.
            'ymin' : List of top left y coordinates.
            'xmax' : List of bottom right x coordinates.
            'ymax' : List of bottom right y coordinates.
            'occluded' : bool. If True, the object was occluded.
            'truncated' : bool. If True, the object was truncated due to being on the boundary.
            'image' : string. Full path to the image file to which this annotation corresponds.
            'height' : float. Height of annotation.
            'width' : float. Width of annotation.
            'aspect_ratio : float. Aspect ratio of annotation (width/height)
         }
        """
        files = self._getfiles(subset)
        output = []
        if len(files['labels']) > 0:
            for imagefile, jsonfile in tqdm(
                    zip(files['images'], files['labels'])):
                annotations, scene, timeofday, weather = self._read_json_single(
                    jsonfile)
                if annotations['category']:
                    annotations['image'] = [imagefile] * len(annotations['xmin'])
                    annotations['scene'] = [scene] * len(annotations['xmin'])
                    annotations['timeofday'] = [timeofday] * len(
                        annotations['xmin'])
                    annotations['weather'] = [weather] * len(annotations['xmin'])
                    annotations['subset'] = [subset] * len(annotations['xmin'])
                    unfolded = []
                    for i in range(len(annotations['xmin'])):
                        temp = dict()
                        for k in annotations.keys():
                            temp[k] = annotations[k][i]
                        unfolded.append(temp)
                    output += unfolded
                else:
                    annotations['image'] = imagefile
                    annotations['scene'] = scene
                    annotations['timeofday'] = timeofday
                    annotations['weather'] = weather
                    annotations['subset'] = subset
                    annotations['category'] = ''
                    annotations['truncated'] = False
                    annotations['height'] = -1.
                    annotations['width'] = -1.
                    annotations['aspect_ratio']= -1.
                    annotations['xmin']= -1.
                    annotations['ymin'] = -1.
                    annotations['xmax'] = -1.
                    annotations['ymax'] = -1.
                    annotations['occluded'] = -1.
                    output.append(annotations)
        return output
