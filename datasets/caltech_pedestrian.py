import glob
import os
from tqdm import tqdm
from natsort import natsort_keygen

from datasets.detection_dataset import DetectionDataset


class CaltechPedestrian(DetectionDataset):
    def __init__(self, base_dir):
        super(CaltechPedestrian, self).__init__('Caltech Pedestrian')
        self._base_dir = base_dir
        self._trainsets = ['set{}'.format(str(x).zfill(2)) for x in range(6)]
        self._testsets = ['set{}'.format(str(x).zfill(2)) for x in range(6, 11)]

    def _getfiles(self, subset):
        """
        Returns the lists of image files and corresponding label files of bdd100K dataset.
        :param base_dir: string. Base folder inside which all the BDD100K dataset is stored.
        :param subset: string. One of 'train', 'test' and 'val'.
        :return: Dict. It has two keys. The key 'images' contains a list of image filenames with full path.
                       The key 'labels' contains a list of label filenames with full path. If subset='test', the key
                       'labels' contains an empty list.
        """
        if not os.path.exists(self._base_dir):
            raise ValueError('The path {} does not exist.'.format(
                self._base_dir))

        if subset not in ['train', 'test']:
            raise ValueError('subset must be one of train, test or  val.')
        else:
            if subset == 'train':
                setname = self._trainsets
            else:
                setname = self._testsets

        natsort_key = natsort_keygen(
            key=lambda x: os.path.splitext(os.path.basename(x))[0])

        image_path = os.path.join(self._base_dir, 'images')

        images = glob.glob(os.path.join(image_path, '*.jpg'))

        valid_images_index = [os.path.basename(x)[:5] in setname for x in images]
        images = [images[ind] for ind, val in enumerate(valid_images_index) if val is True]
        images.sort(key=natsort_key)

        label_path = os.path.join(self._base_dir, 'annotations')
        labels = glob.glob(os.path.join(label_path, '*.txt'))
        valid_labels_index = [os.path.basename(x)[:5] in setname for x in labels]

        labels = [labels[ind] for ind, val in enumerate(valid_labels_index) if val is True]
        labels.sort(key=natsort_key)

        output = {
            'images': images,
            'labels': labels
        }
        return output

    @staticmethod
    def _read_annotation_single(annotation_file):
        """
        Reads a caltech annotation file and stores a dictionary.
        :param annotation_file: Annotation file containing caltech groundtruth
        :return: A dictionary with following keys
        'category' : Name of the object category
        'occluded' : A number between 0 and 1. Denotes the amount of occlusion.
        'height' : Height of the bounding box
        'width' : width of the bounding box.
        'height_vis' : height of the visible portion of the bounding box
        'width_vis' : width of the visible portion of the bounding box
        'aspect_ratio : Aspect ratio of the object (width/height)
        'xmin' : Top left corner ( x coordinate) of the bounding box
        'ymin' : Top left corner (y coordinate) of the bounding box
        'xmax' : Bottom right corner (x coordinate of the bounding box
        'ymax' : Bottom right corner (y coordinate of the bounding box
        'xmin_vis', 'ymin_vis', 'xmax_vis', 'ymax_vis' : Same as xmin, ymin, xmax, ymax
        but only for the visible part of the bounding box. If the person is not occluded, then these
        will be the same as xmin, ymin, xamx , ymax.
        """

        annotations = {
            'category': [],
            'occluded': [],
            'height': [],
            'width': [],
            'height_vis': [],
            'width_vis': [],
            'aspect_ratio': [],
            'xmin': [],
            'ymin': [],
            'xmax': [],
            'ymax': [],
            'xmin_vis': [],
            'ymin_vis': [],
            'xmax_vis': [],
            'ymax_vis': []
        }

        for line in open(annotation_file, 'r'):
            line = line.strip()
            line = line.split(' ')
            if line[0] == '%':
                continue
            else:
                gt = line[1:5]
                gt = list(map(float, gt))
                xmin, ymin, width, height = gt
                if not width:
                    continue
                xmax = xmin + width - 1
                ymax = ymin + height - 1
                gt_vis = line[6:10]
                gt_vis = list(map(float, gt_vis))
                if all([x == 0 for x in gt_vis]):
                    xmin_vis = xmin
                    ymin_vis = ymin
                    xmax_vis = xmax
                    ymax_vis = ymax
                    height_vis = height
                    width_vis = width
                else:
                    xmin_vis, ymin_vis, width_vis, height_vis = gt_vis
                    xmax_vis = xmin_vis + width_vis - 1
                    ymax_vis = ymin_vis + height_vis - 1

                occluded = float(width_vis * height_vis) / float(width * height)
                occluded = 1. - occluded
                annotations['category'].append(line[0])
                annotations['occluded'].append(occluded)
                annotations['height'].append(height)
                annotations['width'].append(width)
                annotations['height_vis'].append(height_vis)
                annotations['width_vis'].append(width_vis)
                annotations['aspect_ratio'].append(float(width) / float(height))
                annotations['xmin'].append(xmin)
                annotations['xmax'].append(xmax)
                annotations['ymin'].append(ymin)
                annotations['ymax'].append(ymax)
                annotations['xmin_vis'].append(xmin_vis)
                annotations['ymin_vis'].append(ymin_vis)
                annotations['xmax_vis'].append(xmax_vis)
                annotations['ymax_vis'].append(ymax_vis)
        return annotations

    def assemble_annotations(self, subset):
        files = self._getfiles(subset)
        output = []
        for imagefile, annotation_file in tqdm(zip(files['images'], files['labels'])):
            annotations = self._read_annotation_single(annotation_file)
            if annotations['category']:
                annotations['image'] = [imagefile] * len(annotations['xmin'])
                annotations['subset'] = [subset] * len(annotations['xmin'])
                unfolded = []
                for i in range(len(annotations['xmin'])):
                    temp = dict()
                    for k in annotations.keys():
                        temp[k] = annotations[k][i]
                    unfolded.append(temp)
                output += unfolded
                
            else:
                annotations['image'] = imagefile
                annotations['subset'] = subset
                annotations['category'] = ''
                annotations['height_vis'] = -1.
                annotations['width_vis'] = -1.
                annotations['aspect_ratio'] = -1.
                annotations['xmin'] = -1.
                annotations['ymin'] = -1.
                annotations['xmax'] = -1.
                annotations['ymax'] = -1.
                annotations['xmin_vis']= -1.
                annotations['ymin_vis'] = -1.
                annotations['xmax_vis'] = -1.
                annotations['ymax_vis'] = -1.
                annotations['occluded'] = -1.
                annotations['height'] = -1.
                annotations['width'] = -1.
                output.append(annotations)
        return output
