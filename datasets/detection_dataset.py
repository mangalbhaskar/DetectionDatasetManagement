import abc


class DetectionDataset(abc.ABC):
    def __init__(self, name):
        self._name = name

    @abc.abstractmethod
    def assemble_annotations(self, **kwargs):
        raise NotImplementedError('This method must be implemented.')
